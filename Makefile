# Please define this in your Makefile
RELATIVE_PATH?=.

# ARDUINO SETTINGS

ARDUINO_DIR=/home/jerome/Documents/Arduino/arduino-1.0.5

ALTERNATE_CORE_PATH=/home/jerome/.arduino15/packages/attiny/hardware/avr/1.0.1
ARDUINO_VAR_PATH =/home/jerome/.arduino15/packages/attiny/hardware/avr/1.0.1/variants

USER_LIB_PATH =/home/jerome/Documents/sketchbook/libraries

BOARD_TAG     ?= attiny
VARIANT = tiny8
BOARD_SUB=ATtiny85
F_CPU=8000000L
MCU=attiny85

ARDUINO_PORT  ?= /dev/ttyACM0

ISP_PROG = usbasp

# RULES

SUBMODULES=$(RELATIVE_PATH)/_submodules
ARDUINO_LIBS=FastLED
LOCAL_CPP_SRCS=$(wildcard *.cpp [!_]*/*.cpp [!_]*/[!_]*/*.cpp)


include $(ARDMK_DIR)/Arduino.mk

