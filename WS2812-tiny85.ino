#include <FastLED.h>

// How many leds in your strip?
#define BRIGHTNESS    96
#define NUM_LEDS      5
#define TEMPO         100

#define DATA_PIN 0

CRGB leds[NUM_LEDS];
float tcount = 0.0;          //-INC VAR FOR SIN LOOPS 
int ibright = 0;             //-BRIGHTNESS (0-255)
int tempo_anim = TEMPO;
uint8_t gHue = 0; // rotating "base color" 

void colorWipe_up (struct CRGB rgb, uint8_t wait) {

    for (int i=0; i < NUM_LEDS; i++) {
        leds[i]=rgb;
        FastSPI_LED.show();
        delay(wait);
    }
}
void colorWipe_down (struct CRGB rgb, uint8_t wait) {

    for (int i =  NUM_LEDS-1 ; i >= 0;  i--) {
        leds[i]=rgb;
        FastSPI_LED.show();
        delay(wait);
    }
}
void black_out(){
  // all leds off
  memset(leds, 0, NUM_LEDS * 3); // all leds off
  FastSPI_LED.show();
}
void dual_color(struct CRGB color1, struct CRGB color2,uint8_t wait) {
  for (int i=0; i < NUM_LEDS; i++) {
    memset(leds, 0, NUM_LEDS * 3); // all leds off
    leds[i]=color1;
    leds[NUM_LEDS-(i+1)]=color2;
    FastSPI_LED.show();
    delay(wait);
  }
}

void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
}

void loop() {
  // put your main code here, to run repeatedly:
    colorWipe_up ( CRGB::Red,tempo_anim); 
    delay(100);
    colorWipe_down ( CRGB::Green,tempo_anim); 
    delay(100);
    colorWipe_up ( CRGB::Purple,tempo_anim); 
    delay(100);
    colorWipe_down ( CRGB::Blue,tempo_anim); 
    delay(100);
    colorWipe_up ( CRGB::Black,tempo_anim); 
    delay(100);
    dual_color( CRGB::LightGreen,CRGB::Aqua,tempo_anim);
    dual_color( CRGB::Aqua,CRGB::LightGreen,tempo_anim);    
    black_out();
    delay(100);

 }


 
